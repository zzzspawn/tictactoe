package GameManager;

public class Move {

    int row, column;
    Toe piece;

    public Move(int row, int column, Toe piece){
        this.row = row;
        this.column = column;
        this.piece = piece;
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

    public Toe getPiece() {
        return piece;
    }
}
