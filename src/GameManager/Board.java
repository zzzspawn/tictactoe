package GameManager;

public class Board {

    Toe[] x1;
    Toe[] x2;
    Toe[] x3;
    boolean tickTurn;
    boolean tackTurn;
    boolean firstTurn;
    boolean firstApproved;

    int xYSize;
    int SquareSize;


    public Board(){
        xYSize = 800;
        SquareSize = xYSize/3;
        x1 = new Toe[3];
        x2 = new Toe[3];
        x3 = new Toe[3];
        firstTurn = true;
        populateRow(x1);
        populateRow(x2);
        populateRow(x3);
        tickTurn = true;
        tackTurn = false;
    }
    public Board getDuplicate() {
        return new Board(xYSize, x1, x2, x3, firstTurn, tickTurn, tackTurn);
    }
    public Board(int xYSize,Toe[] x1, Toe[] x2, Toe[] x3, boolean firstTurn, boolean tickTurn, boolean tackTurn){
        this.xYSize = xYSize;
        SquareSize = xYSize/3;
        this.x1 = x1;
        this.x2 = x2;
        this.x3 = x3;
        this.firstTurn = firstTurn;
        this.tickTurn = tickTurn;
        this.tackTurn = tackTurn;
    }

    public int getBoardSize(){
        return xYSize;
    }

    public int getTileSize() {
        return SquareSize;
    }

    public boolean put(Move move){
        return put(move.getRow(),move.getColumn(),move.getPiece());
    }
    //returns true if piece is placed, and false if invalid position, or if another piece is occupying the space or returns false if it is not this players turn
    public boolean put(int row, int column, Toe piece){
        System.out.println("firstTurn: " + firstTurn);
        System.out.println("tickTurn: " + tickTurn);
        System.out.println("tackTurn: " + tackTurn);
        System.out.println("Trying to place: " + piece.getType() + " in row: " + row + ", and column: " + column);
        firstApproved = false;

        if(firstTurn) {
            if (piece.getType().equals("Tack")){
            firstTurn = false;
            tickTurn = true;
            tackTurn = false;
            }else if(piece.getType().equals("Tick")){
                firstTurn = false;
                tickTurn = false;
                tackTurn = true;
            }
        }else {

            if(piece.getType().equals("Tack")){
                if(!tackTurn){
                    return false;
                }else {
                    tackTurn = false;
                    tickTurn = true;
                    firstApproved = true;
                }
            }

            if(piece.getType().equals("Tick")){
                if(!tickTurn){
                    return false;
                }else {
                    tackTurn = true;
                    tickTurn = false;
                    firstApproved = true;
                }
            }

        }
        if (row == 0){
            if(x1[column].getType().equals("None")){
                x1[column] = piece;
                nextPiece(piece);
                return true;
            }else {
                if (firstApproved){
                    nextPiece(piece.getOppositeType());
                }
                return false;
            }
        }else if(row == 1){
            if(x2[column].getType().equals("None")){
                x2[column] = piece;
                nextPiece(piece);
                return true;
            }else {
                if (firstApproved){
                    nextPiece(piece.getOppositeType());
                }
                return false;
            }

        }else if (row == 2){
            if(x3[column].getType().equals("None")){
                x3[column] = piece;
                nextPiece(piece);
                return true;
            }else {
                if (firstApproved){
                    nextPiece(piece.getOppositeType());
                }
                return false;
            }
        }else {
            return false;
        }
    }

    private void nextPiece(Toe piece) {
        if(piece.getType().equals("Tack")){
                tackTurn = false;
                tickTurn = true;
        }
        if(piece.getType().equals("Tick")){
                tackTurn = true;
                tickTurn = false;
        }
    }

    public Toe[] getRow(int row){
        if (row == 0){
            return x1;
        }else if (row == 1){
            return x2;
        }else if (row == 2){
            return x3;
        }else {
            return null;
        }
    }

    public void populateRow(Toe[] row){
        int i = 0;
        while (i < row.length){
            row[i] = new None();
            i++;
        }
    }

    public String whosTurn(){
        if(tickTurn){
            return "tick";
        }else if(tackTurn){
            return "tack";
        }else {
            System.out.println("Something went wrong");
            return "something Went Wrong";
        }
    }


}
