package GameManager;

public class WinCheck {

    String winner;
    int[] winnerRow;

    public boolean GameOverCheck(Board board){

        Toe[] row0 = board.getRow(0);
        Toe[] row1 = board.getRow(1);
        Toe[] row2 = board.getRow(2);
        winnerRow = new int[3];

        //X X X
        //O O O
        //O O O
        if( row0[0].getType().equals(row0[1].getType()) && row0[1].getType().equals(row0[2].getType()) && !(row0[0].getType().equals("None")) ){
            winner = row0[0].getType();
            winnerRow[0] = 1;
            winnerRow[1] = 2;
            winnerRow[2] = 3;
            return true;
        }

        //O O O
        //X X X
        //O O O
        if ( row1[0].getType().equals(row1[1].getType()) && row1[1].getType().equals(row1[2].getType()) && !(row1[0].getType().equals("None")) ){
            winner = row0[0].getType();
            winnerRow[0] = 4;
            winnerRow[1] = 5;
            winnerRow[2] = 6;
            return true;
        }

        //O O O
        //O O O
        //X X X
        if ( row2[0].getType().equals(row2[1].getType()) && row2[1].getType().equals(row2[2].getType()) && !(row2[0].getType().equals("None")) ){
            winner = row0[0].getType();
            winnerRow[0] = 7;
            winnerRow[1] = 8;
            winnerRow[2] = 9;
            return true;
        }

        //X O O
        //X O O
        //X O O
        if( row0[0].getType().equals(row1[0].getType()) && row1[0].getType().equals(row2[0].getType()) && !(row0[0].getType().equals("None")) ){
            winner = row0[0].getType();
            winnerRow[0] = 1;
            winnerRow[1] = 4;
            winnerRow[2] = 7;
            return true;
        }

        //O X O
        //O X O
        //O X O
        if( row0[1].getType().equals(row1[1].getType()) && row1[1].getType().equals(row2[1].getType()) && !(row0[1].getType().equals("None")) ){
            winner = row0[0].getType();
            winnerRow[0] = 2;
            winnerRow[1] = 5;
            winnerRow[2] = 8;
            return true;
        }

        //O O X
        //O O X
        //O O X
        if( row0[2].getType().equals(row1[2].getType()) && row1[2].getType().equals(row2[2].getType()) && !(row0[2].getType().equals("None")) ){
            winner = row0[0].getType();
            winnerRow[0] = 3;
            winnerRow[1] = 6;
            winnerRow[2] = 9;
            return true;
        }

        //X O O
        //O X O
        //O O X
        if( row0[0].getType().equals(row1[1].getType()) && row1[1].getType().equals(row2[2].getType()) && !(row0[0].getType().equals("None")) ){
            winner = row0[0].getType();
            winnerRow[0] = 1;
            winnerRow[1] = 5;
            winnerRow[2] = 9;
            return true;
        }

        //O O X
        //O X O
        //X O O
        if( row0[2].getType().equals(row1[1].getType()) && row1[1].getType().equals(row2[0].getType()) && !(row0[2].getType().equals("None")) ){
            winner = row0[0].getType();
            winnerRow[0] = 3;
            winnerRow[1] = 5;
            winnerRow[2] = 7;
            return true;
        }

        return false;

    }

    public boolean gameOverNoWinnerCheck(Board board){

        Toe[] row0 = board.getRow(0);
        Toe[] row1 = board.getRow(1);
        Toe[] row2 = board.getRow(2);

        boolean foundStandoff = true;
        int i = 0;
        while (foundStandoff && i < row0.length){
            if(row0[i].getType().equals("None")){
                foundStandoff = false;
            }
            if(row1[i].getType().equals("None")){
                foundStandoff = false;
            }
            if(row2[i].getType().equals("None")){
                foundStandoff = false;
            }
            i++;
        }
        return foundStandoff;
    }

    public String getWinner(){

        if(winner.equals("Tack")){
            return "tac";
        }else{
            return "tic";
        }

    }

    public int[] getWinnerRow(){
        return winnerRow;
    }


}
