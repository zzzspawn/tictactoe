package GameManager;

public class Tack extends Toe{

    @Override
    public String getType(){
        return "Tack";
    }

    @Override
    public Toe getOppositeType(){
        return new Tick();
    }

}
