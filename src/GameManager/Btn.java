package GameManager;

public class Btn {

    String ImageLoc;
    int locX;
    int locY;
    int xSize;
    int ySise;

    public Btn(String location, int x, int y){
        ImageLoc = location;
        locX = x;
        locY = y;
        xSize = 100;
        ySise = 50;
    }

    public int getLocX(){
        return locX;
    }

    public int getLocY() {
        return locY;
    }

    public String getImageLoc() {
        return ImageLoc;
    }

    public int getxSize() {
        return xSize;
    }

    public int getySise() {
        return ySise;
    }
}
