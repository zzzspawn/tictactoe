package GameManager;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

public class Host extends Thread {

    private DatagramSocket socket;
    private boolean running;
    private byte[] buf = new byte[256];

    public Host() throws SocketException {
        socket = new DatagramSocket(4445);
    }

    public void run() {
        running = true;

        while (running) {
            DatagramPacket packet
                    = new DatagramPacket(buf, buf.length);
            try {
                socket.receive(packet);
                System.out.println("packet length: " + packet.getLength());
                System.out.println("packet offset: " + packet.getOffset());

                InetAddress address = packet.getAddress();
                int port = packet.getPort();

                String received = new String(packet.getData(), packet.getOffset(), packet.getLength());

                packet = new DatagramPacket(buf, buf.length, address, port);


                if (received.equals("end")) {
                    running = false;
                    continue;
                }
                if(running) {
                    DatagramPacket packetBack;
                    System.out.println("String received from client: ");
                    System.out.println(received);
                    System.out.println("length of received from client: ");
                    System.out.println(received.length());

                    if (received.equals("Hello Server")) {
                        byte[] buf = "Hello Client".getBytes();
                        packetBack = new DatagramPacket(buf, buf.length, address, port);
                        System.out.println("Sending Packet back");
                        socket.send(packetBack);
                    } else {
                        System.out.println("Sending exact Packet back");
                        socket.send(packet);
                    }
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        socket.close();
    }
}
