package GameManager;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

import java.io.File;
import java.net.SocketException;
import java.net.UnknownHostException;

public class Game extends Application {

    Board board;
    WinCheck winCheck;
    Toe tick;
    Toe tack;
    boolean safeMove;
    Square square;
    boolean gameOver;
    Btn resetbtn;
    //Btn hostbtn;
    //Btn searchbtn;
    //Client client;
    public static void main(String[] args){
        launch(args);
    }

 
    public void start(Stage theStage)
    {

        board = new Board();
        winCheck = new WinCheck();
        gameOver = false;
        tick = new Tick();
        tack = new Tack();
        safeMove = false;
        theStage.setTitle( "Tic Tac Toe!" );
        theStage.setResizable(false);
        square = new Square(board.getTileSize());
        resetbtn = new Btn("media/reset.png", square.getSize()*3+(square.getSize()/2-50), square.getSize()*2+(square.getSize()/2-25));
        //hostbtn = new Btn("media/host.png", (square.getSize()*3+(square.getSize()/2-50)), (square.getSize()*2+(square.getSize()/2-25))-50);
        //searchbtn = new Btn("media/search.png", (square.getSize()*3+(square.getSize()/2-50)), (square.getSize()*2+(square.getSize()/2-25))-100);
        Group root = new Group();
        Scene theScene = new Scene( root );
        theStage.setScene( theScene );

        Canvas canvas = new Canvas( board.getBoardSize()+square.getSize(), board.getBoardSize() );

        root.getChildren().add( canvas );




        theScene.setOnMouseClicked(
                new EventHandler<MouseEvent>()
                {
                    public void handle(MouseEvent e) {
                        if(!gameOver){
                        //if mouse is in placement and is clicked
                        if ((e.getX() < square.getSize() && e.getY() < square.getSize())) {
                            //System.out.println("first square");
                            String turn = board.whosTurn();
                            safeMove = false;
                            if (turn.equals("tick")) {
                                safeMove = board.put(0, 0, tick);
                            } else {
                                safeMove = board.put(0, 0, tack);
                            }
                            if (safeMove) {
                                System.out.println("placed in first square");
                            }
                        } else if ((e.getX() < square.getSize() && e.getY() < square.getSize() * 2 && e.getY() > square.getSize())) {
                            //System.out.println("fourth square");
                            String turn = board.whosTurn();
                            safeMove = false;
                            if (turn.equals("tick")) {
                                safeMove = board.put(1, 0, tick);
                            } else {
                                safeMove = board.put(1, 0, tack);
                            }
                            if (safeMove) {
                                System.out.println("placed in fourth square");
                            }
                        } else if ((e.getX() < square.getSize() && e.getY() < square.getSize() * 3 && e.getY() > square.getSize() * 2)) {
                            //System.out.println("seventh square");
                            String turn = board.whosTurn();
                            safeMove = false;
                            if (turn.equals("tick")) {
                                safeMove = board.put(2, 0, tick);
                            } else {
                                safeMove = board.put(2, 0, tack);
                            }
                            if (safeMove) {
                                System.out.println("placed in seventh square");
                            }
                        } else if (e.getX() < square.getSize() * 2 && e.getX() > square.getSize() && e.getY() < square.getSize()) {
                            //System.out.println("second square");
                            String turn = board.whosTurn();
                            safeMove = false;
                            if (turn.equals("tick")) {
                                safeMove = board.put(0, 1, tick);
                            } else {
                                safeMove = board.put(0, 1, tack);
                            }
                            if (safeMove) {
                                System.out.println("placed in second square");
                            }
                        } else if (e.getX() < square.getSize() * 2 && e.getX() > square.getSize() && e.getY() < square.getSize() * 2 && e.getY() > square.getSize()) {
                            //System.out.println("fifth square");
                            String turn = board.whosTurn();
                            safeMove = false;
                            if (turn.equals("tick")) {
                                safeMove = board.put(1, 1, tick);
                            } else {
                                safeMove = board.put(1, 1, tack);
                            }
                            if (safeMove) {
                                System.out.println("placed in fifth square");
                            }
                        } else if (e.getX() < square.getSize() * 2 && e.getX() > square.getSize() && e.getY() < square.getSize() * 3 && e.getY() > square.getSize() * 2) {
                            //System.out.println("eight square");
                            String turn = board.whosTurn();
                            safeMove = false;
                            if (turn.equals("tick")) {
                                safeMove = board.put(2, 1, tick);
                            } else {
                                safeMove = board.put(2, 1, tack);
                            }
                            if (safeMove) {
                                System.out.println("placed in eight square");
                            }
                        } else if (e.getX() < square.getSize() * 3 && e.getX() > square.getSize() * 2 && e.getY() < square.getSize()) {
                            //System.out.println("third square");
                            String turn = board.whosTurn();
                            safeMove = false;
                            if (turn.equals("tick")) {
                                safeMove = board.put(0, 2, tick);
                            } else {
                                safeMove = board.put(0, 2, tack);
                            }
                            if (safeMove) {
                                System.out.println("placed in third square");
                            }
                        } else if (e.getX() < square.getSize() * 3 && e.getX() > square.getSize() * 2 && e.getY() < square.getSize() * 2 && e.getY() > square.getSize()) {
                            //System.out.println("sixth square");
                            String turn = board.whosTurn();
                            safeMove = false;
                            if (turn.equals("tick")) {
                                safeMove = board.put(1, 2, tick);
                            } else {
                                safeMove = board.put(1, 2, tack);
                            }
                            if (safeMove) {
                                System.out.println("placed in sixth square");
                            }
                        } else if (e.getX() < square.getSize() * 3 && e.getX() > square.getSize() * 2 && e.getY() < square.getSize() * 3 && e.getY() > square.getSize() * 2) {
                            //System.out.println("ninth square");
                            String turn = board.whosTurn();
                            safeMove = false;
                            if (turn.equals("tick")) {
                                safeMove = board.put(2, 2, tick);
                            } else {
                                safeMove = board.put(2, 2, tack);
                            }
                            if (safeMove) {
                                System.out.println("placed in ninth square");
                            }
                        }
                        if (winCheck.GameOverCheck(board) || winCheck.gameOverNoWinnerCheck(board)) {
                            gameOver = true;
                        }
                    }

                    if(e.getX() > resetbtn.getLocX() && e.getX() < resetbtn.getLocX()+ resetbtn.getxSize() && e.getY() > resetbtn.getLocY() && e.getY() < resetbtn.getLocY()+ resetbtn.getySise()){
                            System.out.println("reset pressed");
                            gameOver = false;
                            board = new Board();
                            winCheck = new WinCheck();
                            //client.sendEcho("end");
                            //client.close();

                    }

                        /*if(e.getX() > hostbtn.getLocX() && e.getX() < hostbtn.getLocX()+ hostbtn.getxSize() && e.getY() > hostbtn.getLocY() && e.getY() < hostbtn.getLocY()+ hostbtn.getySise()){
                            System.out.println("Host button was pressed");

                            try {
                                new Host().start();
                            } catch (SocketException ee) {
                                ee.printStackTrace();
                            }

                        }*/

                        /*if(e.getX() > searchbtn.getLocX() && e.getX() < searchbtn.getLocX()+ searchbtn.getxSize() && e.getY() > searchbtn.getLocY() && e.getY() < searchbtn.getLocY()+ searchbtn.getySise()){
                            System.out.println("Search button was pressed");
                            try {
                            client = new Client();
                            } catch (SocketException | UnknownHostException ee) {
                                ee.printStackTrace();
                            }
                            String echo = client.sendEcho("Hello Server");
                            if(echo.equals("Hello Client")){
                                System.out.println("got the message back");
                            }else {
                                System.out.println("bad response");
                                System.out.println("response was: ");
                                System.out.println(echo);
                            }

                        }*/

                    }

                });

        GraphicsContext gc = canvas.getGraphicsContext2D();


        gc.setLineWidth(1);




        new AnimationTimer()
        {
            public void handle(long currentNanoTime)
            {
                // Clear the canvas
                gc.setFill( Color.web("#d6d6d6") );
                gc.fillRect(0,0, square.getSize(),square.getSize());

                gc.setFill( Color.web("#707070") );
                gc.fillRect(0,square.getSize(), square.getSize(),square.getSize());

                gc.setFill( Color.web("#d6d6d6") );
                gc.fillRect(0,square.getSize()*2, square.getSize(),square.getSize());

                gc.setFill( Color.web("#707070") );
                gc.fillRect(square.getSize(),0, square.getSize(),square.getSize());

                gc.setFill( Color.web("#d6d6d6") );
                gc.fillRect(square.getSize(),square.getSize(), square.getSize(),square.getSize());

                gc.setFill( Color.web("#707070") );
                gc.fillRect(square.getSize(),square.getSize()*2, square.getSize(),square.getSize());

                gc.setFill( Color.web("#d6d6d6") );
                gc.fillRect(square.getSize()*2,0, square.getSize(),square.getSize());

                gc.setFill( Color.web("#707070") );
                gc.fillRect(square.getSize()*2,square.getSize(), square.getSize(),square.getSize());

                gc.setFill( Color.web("#d6d6d6") );
                gc.fillRect(square.getSize()*2,square.getSize()*2, square.getSize(),square.getSize());

                //sidepanel
                gc.setFill( Color.web("#8edd7c") );
                gc.fillRect(square.getSize()*3,0, square.getSize(),square.getSize()*3);

                gc.setFill( Color.BLUE );

                drawShapes(gc);
                drawBoard(gc);
                drawText(gc);
                try {

                    File restartFile = new File(resetbtn.getImageLoc());
                    Image restart   = new Image( restartFile.toURI().toURL().toString());
                    gc.drawImage( restart, resetbtn.getLocX(), resetbtn.getLocY());

                    //File hostFile = new File(hostbtn.getImageLoc());
                    //Image host   = new Image( hostFile.toURI().toURL().toString());
                    //gc.drawImage( host, hostbtn.getLocX(), hostbtn.getLocY());

                    //File searchFile = new File(searchbtn.getImageLoc());
                    //Image search   = new Image( searchFile.toURI().toURL().toString());
                    //gc.drawImage( search, searchbtn.getLocX(), searchbtn.getLocY());


                }catch (Exception e){
                    e.printStackTrace();
                }

                if(gameOver){
                    drawWinnerText(gc);
                }

            }
        }.start();

        theStage.show();
    }

    private void drawWinnerText(GraphicsContext gc) {

        Font theFont = Font.font( "Helvetica", FontWeight.BOLD, 24 );
        gc.setFont( theFont );
        gc.setStroke( Color.BLACK );
        String pointsText = "";
        if(winCheck.GameOverCheck(board) || !winCheck.gameOverNoWinnerCheck(board)) {
            String winner = winCheck.getWinner();
            pointsText = winner + " won the game!";
        }else {
            pointsText = "No one" + " won the game!";
        }
        gc.fillText(pointsText, square.getSize() * 3 + 5, 200);

    }

    private void drawShapes(GraphicsContext gc) {
        gc.setStroke(Color.BLACK);
        gc.setLineWidth(5);
        String shape = board.whosTurn();
        if(shape.equals("tick")){
            gc.setStroke(Color.RED);
            gc.strokeLine(square.getSize()*3 + 40, 10, square.getSize()*3 + 10, 40);
            gc.strokeLine(square.getSize()*3 + 10, 10, square.getSize()*3 + 40, 40);
            gc.setStroke(Color.BLACK);
            gc.strokeOval(square.getSize()*3 + 10, 60, 30, 30);
        }else {
            gc.setStroke(Color.BLACK);
            gc.strokeLine(square.getSize()*3 + 40, 10, square.getSize()*3 + 10, 40);
            gc.strokeLine(square.getSize()*3 + 10, 10, square.getSize()*3 + 40, 40);
            gc.setStroke(Color.RED);
            gc.strokeOval(square.getSize()*3 + 10, 60, 30, 30);
        }
        gc.setStroke(Color.BLACK);

    }

    private void drawText(GraphicsContext gc) {
        Font theFont = Font.font( "Helvetica", FontWeight.BOLD, 24 );
        gc.setFont( theFont );
        gc.setStroke( Color.BLACK );
        String pointsText = "'s turn";
        String shape = board.whosTurn();
        if(shape.equals("tick")) {
            gc.fillText(pointsText, square.getSize() * 3 + 50, 30);
        }else {
            gc.fillText(pointsText, square.getSize() * 3 + 50, 85);
    }
        //gc.strokeText( pointsText, 360, 36 );
    }

    private void drawBoard(GraphicsContext gc) {
        gc.setStroke(Color.BLACK);
        Toe[] x0 = board.getRow(0);
        Toe[] x1 = board.getRow(1);
        Toe[] x2 = board.getRow(2);
        int[] winnerRow = null;
        if(winCheck.GameOverCheck(board)){
            winnerRow = winCheck.getWinnerRow();
        }

        if(winnerRow != null){
            if(winnerRow[0] == 1 || winnerRow[1] == 1 || winnerRow[2] == 1){
                gc.setStroke(Color.RED);
            }else {
                gc.setStroke(Color.BLACK);
            }
        }

        if(x0[0].getType().equals("Tick")){
            drawX(gc,0, 0);
        }else if(x0[0].getType().equals("Tack")){
            drawO(gc,0, 0);
        }

        if(winnerRow != null){
            if(winnerRow[0] == 2 || winnerRow[1] == 2 || winnerRow[2] == 2){
                gc.setStroke(Color.RED);
            }else {
                gc.setStroke(Color.BLACK);
            }
        }

        if(x0[1].getType().equals("Tick")){
            drawX(gc,square.getSize(), 0);
        }else if(x0[1].getType().equals("Tack")){
            drawO(gc,square.getSize(), 0);
        }

        if(winnerRow != null){
            if(winnerRow[0] == 3 || winnerRow[1] == 3 || winnerRow[2] == 3){
                gc.setStroke(Color.RED);
            }else {
                gc.setStroke(Color.BLACK);
            }
        }

        if(x0[2].getType().equals("Tick")){
            drawX(gc,square.getSize()*2, 0);
        }else if(x0[2].getType().equals("Tack")){
            drawO(gc,square.getSize()*2, 0);
        }


        if(winnerRow != null){
            if(winnerRow[0] == 4 || winnerRow[1] == 4 || winnerRow[2] == 4){
                gc.setStroke(Color.RED);
            }else {
                gc.setStroke(Color.BLACK);
            }
        }

        if(x1[0].getType().equals("Tick")){
            drawX(gc,0, square.getSize());
        }else if(x1[0].getType().equals("Tack")){
            drawO(gc,0, square.getSize());
        }

        if(winnerRow != null){
            if(winnerRow[0] == 5 || winnerRow[1] == 5 || winnerRow[2] == 5){
                gc.setStroke(Color.RED);
            }else {
                gc.setStroke(Color.BLACK);
            }
        }

        if(x1[1].getType().equals("Tick")){
            drawX(gc,square.getSize(), square.getSize());
        }else if(x1[1].getType().equals("Tack")){
            drawO(gc,square.getSize(), square.getSize());
        }

        if(winnerRow != null){
            if(winnerRow[0] == 6 || winnerRow[1] == 6 || winnerRow[2] == 6){
                gc.setStroke(Color.RED);
            }else {
                gc.setStroke(Color.BLACK);
            }
        }

        if(x1[2].getType().equals("Tick")){
            drawX(gc,square.getSize()*2, square.getSize());
        }else if(x1[2].getType().equals("Tack")){
            drawO(gc,square.getSize()*2, square.getSize());
        }


        if(winnerRow != null){
            if(winnerRow[0] == 7 || winnerRow[1] == 7 || winnerRow[2] == 7){
                gc.setStroke(Color.RED);
            }else {
                gc.setStroke(Color.BLACK);
            }
        }

        if(x2[0].getType().equals("Tick")){
            drawX(gc,0, square.getSize()*2);
        }else if(x2[0].getType().equals("Tack")){
            drawO(gc,0, square.getSize()*2);
        }

        if(winnerRow != null){
            if(winnerRow[0] == 8 || winnerRow[1] == 8 || winnerRow[2] == 8){
                gc.setStroke(Color.RED);
            }else {
                gc.setStroke(Color.BLACK);
            }
        }

        if(x2[1].getType().equals("Tick")){
            drawX(gc,square.getSize(), square.getSize()*2);
        }else if(x2[1].getType().equals("Tack")){
            drawO(gc,square.getSize(), square.getSize()*2);
        }


        if(winnerRow != null){
            if(winnerRow[0] == 9 || winnerRow[1] == 9 || winnerRow[2] == 9){
                gc.setStroke(Color.RED);
            }else {
                gc.setStroke(Color.BLACK);
            }
        }

        if(x2[2].getType().equals("Tick")){
            drawX(gc,square.getSize()*2, square.getSize()*2);
        }else if(x2[2].getType().equals("Tack")){
            drawO(gc,square.getSize()*2, square.getSize()*2);
        }


    }
    public void drawX(GraphicsContext gc, int topLeftX, int topLeftY){
        //gc.setStroke(Color.BLACK);
        gc.setLineWidth(5);
        gc.strokeLine(topLeftX + square.getSize()-20, topLeftY + 10, topLeftX + 10, topLeftY + square.getSize()-20);
        gc.strokeLine(topLeftX+10, topLeftY+10, topLeftX + square.getSize()-20, topLeftY + square.getSize()-20);
    }

    public void drawO(GraphicsContext gc, int topLeftX, int topLeftY){
        //gc.setStroke(Color.BLACK);
        gc.setLineWidth(5);
        gc.strokeOval(topLeftX+10, topLeftY+10, square.getSize()-20, square.getSize()-20);
    }

}
