package GameManager;

public class Tick extends Toe{

    @Override
    public String getType(){
        return "Tick";
    }

    @Override
    public Toe getOppositeType(){
        return new Tack();
    }

}
