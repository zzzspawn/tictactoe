package GameManager;

public class Opponent {

    Board board;
    Toe type;
    Board testBoard;

    public Move nextMove(Board board) {


        testBoard = null;

        boolean satisfied = false;
        while (!satisfied) {
            satisfied = true; //test alle muligheter
        }

        return null;
    }

    private Move getMove(Board board) {

        String me = board.whosTurn();
        Tick tick = new Tick();
        Tack tack = new Tack();
        if (me.equals(tick.getType())) {
            this.type = tick;
        } else {
            this.type = tack;
        }

        Toe[] row0 = board.getRow(0);
        Toe[] row1 = board.getRow(0);
        Toe[] row2 = board.getRow(0);
        int i = 0;
        while (i < row0.length) {
            if (row0[i] == null) {
                return new Move(0, i, type);
            }
            i++;
        }
        i = 0;
        while (i < row1.length) {
            if (row1[i] == null) {
                return new Move(1, i, type);
            }
            i++;
        }
        i = 0;
        while (i < row2.length) {
            if (row2[i] == null) {
                return new Move(2, i, type);
            }
            i++;
        }

return null;
    }
    private boolean tryMove(Board testBoard, Move move){
        WinCheck winCheck = new WinCheck();
        if(testBoard == null) {
            testBoard = board.getDuplicate();
        }
        testBoard.put(move);
        if(winCheck.GameOverCheck(testBoard)){
            return true;
        }else {
            return false;
        }
    }


}
